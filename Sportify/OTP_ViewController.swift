//
//  OTP_ViewController.swift
//  Sportify
//
//  Created by Amit on 02/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class OTP_ViewController: UIViewController, UITextFieldDelegate,AccountEngine_Delegate {

    @IBOutlet var otpTextField:PaddingTextField!
 
    var signupData:SignupData?
    var accountEngine: AccountEngine!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountEngine  = AccountEngine(delegate_: self)
        print(signupData)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true;
    }
    
    //MARK:- UITextField Delegate Method
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cStringUsingEncoding(NSUTF8StringEncoding)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if  textField.text?.length > 3 {
            return false
        } else {
            if textField.text?.length == 3 {
               let isRequested = self.requestForOtpVerification(string)
                if isRequested {
                    textField.resignFirstResponder()
                    return true
                }
            }
        }
        return true
    }
    
    func requestForOtpVerification(string:String) -> Bool {

        var serverOtp = ""
        var customerID = 0
        if let theSignupData = self.signupData {
            serverOtp = theSignupData.otp!
            guard theSignupData.customerID != nil else {
                return false
            }
            customerID = theSignupData.customerID!
        }
        
        let inputOtp = self.otpTextField.text! + string
        if serverOtp == inputOtp {
                KVNProgress.show()
                accountEngine.verifyOTP(userID:customerID , otpCode: inputOtp)
            return true
        } else {
           return false
        }
    }
    
    //MARK:- IBAction Method
    @IBAction func submitClicked(sender:UIButton) {
        self.view.endEditing(true)
        self.requestForOtpVerification("")
    }
    
    @IBAction func backClicked(sender:UIButton) {
        self.resignFirstResponder()

        self.navigationController?.popViewControllerAnimated(true);
    }
    
    
    //MARK:- Account Engine Delegate methods
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:AccountEngineRequestType) {
        
        switch requestType {
        case .otpVerification:
            KVNProgress.dismissWithCompletion({
                UIAlertController.present(title: "", message: "Successfully Login", actionTitles: ["OK"]) { (action) -> () in
                
                self.navigationController?.popToRootViewControllerAnimated(true)
                }
            })
            break
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse:SportifyError, requestType:AccountEngineRequestType) {
        
        switch requestType {
            
        case .otpVerification:
            KVNProgress.dismissWithCompletion({
                UIAlertController.present(title: "Error", message: errorResponse.erroMessage, actionTitles: ["OK"]) { (action) -> () in }
            })
            break
            
        default:
            break
        }
    }

    
}
