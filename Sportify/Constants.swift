//
//  Constants.swift
//  Sportify
//
//  Created by Amit on 13/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    struct Sportify {
        
    }
    
    struct Path {
        static let Documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        static let Tmp = NSTemporaryDirectory()
    }
    
    struct NetworkConnection {
        static let BaseUrl = "http://devtest.isportifyapp.com:8088/"
        
        static let facebookLoginOrSignup = "webservice/customer/facebookLoginSignup"
        static let signup = "webservice/customer/signup"
        static let otpVerification = "webservice/customer/verifyOtp"
        static let login = "webservice/customer/login"
    }
    
    
    struct Key {
        static let ApiKey = ""
    }
    
    struct Message  {
        static let somethingWrongError = "Something is going wrong. Please try again."
        
    }
    
    struct DomainName {
        static let signup = "Signup"
        static let otpVerification = "Verify OTP"
        static let login = "Login"
    }
    
}

