//
//  Signup.swift
//  Sportify
//
//  Created by Amit on 16/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import ObjectMapper

class Signup: Mappable {

    var data: SignupData?
    var error: ResponseError?
    var isSignupActive:Bool?
    var message: String?
    var status:Bool?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        
        data <- map["data"]
        error <- map["error"]
        isSignupActive <- map["isSignupActive"]
        message <- map["message"]
        status <- map["status"]
        message <- map["message"]
    }
}

class SignupData: Mappable {
    
    var customerID: Int?
    var otp: String?

    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        customerID <- map["customerId"]
        otp <- map["customerOtp"]
    }
}



