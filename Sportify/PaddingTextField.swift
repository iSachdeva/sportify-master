//
//  PaddingTextField.swift
//  Sportify
//
//  Created by Amit on 30/05/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//


import UIKit

class PaddingTextField: UITextField {
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    @IBInspectable var leftInset: CGFloat = 0
    
    @IBInspectable var leftImageHeight: CGFloat = 0
    @IBInspectable var leftImageWidth: CGFloat = 0
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectMake(bounds.origin.x + paddingLeft, bounds.origin.y, bounds.size.width - paddingLeft - paddingRight, bounds.size.height);
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return textRectForBounds(bounds)
    }
    
    @IBInspectable var icon:UIImage? { didSet {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        imageView.image = icon
        self.leftView = imageView
        self.leftViewMode = .Always
        }
    }
    
    override func leftViewRectForBounds(bounds: CGRect) -> CGRect {
        var height:CGFloat = 0
        var width:CGFloat = 0
        if let leftView = self.leftView {
            height = leftView.bounds.height
            width = leftView.bounds.width
        }
        return CGRect(x: leftInset, y: bounds.height/2 - height/2, width: width, height: height)
    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
      
        if action == #selector(NSObject.cut(_:)) {
            return false
        }
        if action == #selector(NSObject.copy(_:)) {
            return false
        }
        if action == #selector(NSObject.paste(_:)) {
            return false
        }
        if action == #selector(NSObject.select(_:)) {
            return false
        }
        if action == #selector(NSObject.selectAll(_:)) {
            return false
        }
        if action == #selector(NSObject.delete(_:)) {
            return false
        }
        if action == #selector(NSObject.select(_:)) {
            return false
        }
        if action == #selector(NSObject.select(_:)) {
            return false
        }
        if action == #selector(NSObject.select(_:)) {
            return false
        }
        if action == #selector(NSObject.select(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
}