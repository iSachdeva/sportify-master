//
//  AccountEngine.swift
//  Sportify
//
//  Created by Amit on 09/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import UIKit

enum AccountEngineRequestType {
    
    case  facebookLogin
    case  signUp
    case  otpVerification
    case  login
}

protocol AccountEngine_Delegate {
    
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:AccountEngineRequestType)
    func didRequestFailedWithErrorResponse(errorResponse:SportifyError, requestType:AccountEngineRequestType)
}

class AccountEngine: NSObject,NetworkConnection_Delegate {
    
    var delegate:AccountEngine_Delegate?
    //  var networkConnection:NetworkConnection!
    
    //MARK:- init()
    init(delegate_:AccountEngine_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    //MARK:- API's
    func loginOrSignupWithFacebook(facebookId:String)  {
        
        let parameters = ["customerFacebookId":facebookId];
        NetworkConnection.init(delegate_: self).requestTologinOrSignupByFacebookID(parameters)
    }
    
    func signup(facebookId:String?, name:String, phoneNumber:String, password:String, email:String?)  {
        
        let parameters = NSMutableDictionary()
        
        if let theFacebookId = facebookId {
            parameters.setObject(theFacebookId, forKey: "customerFacebookId")
        } else {
            parameters.setObject("", forKey: "customerFacebookId")
        }
        
        if let theEmail = email {
            parameters.setObject(theEmail, forKey: "")
        } else {
            parameters.setObject("", forKey: "customerEmail")
        }
        
        parameters.setObject(name, forKey: "customerName")
        var phoneNumberStr = phoneNumber
        phoneNumberStr.deleteCharactersInRange(NSMakeRange(0,3))
        parameters.setObject(phoneNumberStr, forKey: "customerMobile")
        parameters.setObject(password, forKey: "customerPassword")
        parameters.setObject("", forKey: "customerProfileImage")
        parameters.setObject("+60", forKey: "customerCountryCode")
        
        NetworkConnection.init(delegate_: self).requestToSignupWithMobileNumber(parameters)
    }
    
    
    func verifyOTP(userID userId:Int, otpCode:String) {
        let parameters = NSMutableDictionary()

        parameters.setObject(userId, forKey: "customerId")
        parameters.setObject(otpCode, forKey: "customerOtp")
      
        NetworkConnection.init(delegate_: self).requestToVerifyOTP(parameters)
    }
    
    func login(phoneNumber phoneNumber:String, password:String) {
        let parameters = NSMutableDictionary()
        
        var phoneNumberStr = phoneNumber
        phoneNumberStr.deleteCharactersInRange(NSMakeRange(0,3))
        parameters.setObject(phoneNumberStr, forKey: "customerMobile")
        parameters.setObject(password, forKey: "customerPassword")
        parameters.setObject("+60", forKey: "customerCountryCode")
        
        NetworkConnection.init(delegate_: self).requestToLogin(parameters)
    }
    
    
    //MARK:- NetworkConnection Delegate
    func networkConnectionDidSuccessWithResponse(response: AnyObject?, requestType: NetworkRequestType) {
        
        switch requestType {
            
        case .loginOrSignUpWithFacebook:
            
            self.delegate?.didRequestFinishedWithSuccessResponse(response!, requestType: .facebookLogin)
            break
            
        case .signUpWithMobileNumber:
            self.delegate?.didRequestFinishedWithSuccessResponse(response!, requestType: .signUp)
            break
            
        case .otpVerification :
            self.delegate?.didRequestFinishedWithSuccessResponse(response!, requestType: .otpVerification)
            break
            
        case .loginWithMobileNumber:
            self.delegate?.didRequestFinishedWithSuccessResponse(response!, requestType: .login)

            break
        }
    }
    
    
    func networkConnectionDidFailedWithErrorResponse(response: SportifyError?, requestType: NetworkRequestType) {
        
        switch requestType {
            
        case .loginOrSignUpWithFacebook:
            
            self.delegate?.didRequestFailedWithErrorResponse(response!,requestType: .facebookLogin)
            break;
            
        case .signUpWithMobileNumber:
            
            self.delegate?.didRequestFailedWithErrorResponse(response!, requestType: .signUp)
            break;
            
        case .otpVerification :
            
            self.delegate?.didRequestFailedWithErrorResponse(response!, requestType: .otpVerification)
            break
            
        case .loginWithMobileNumber:
            
            self.delegate?.didRequestFailedWithErrorResponse(response!, requestType: .login)
            break
            
        }
    }
    
    
    //MARK:- User input validation methods
    func validationForSignup(name name:String?,phoneNumber:String?, password:String?)  -> (isSuccess: Bool, error: String?)  {
      
        var errorString:String? = ""
        
        guard let nameText = name where nameText.length > 0 else {
            errorString = "Please input your name"
            return (false, errorString)
        }
        guard let phoneNumberText = phoneNumber where phoneNumberText.length > 0 else {
            errorString = "Please enter your phone number"
            return (false, errorString)
        }
        if phoneNumberText.length > 0 && phoneNumberText.length < 9 {
            errorString = "Please enter a valid phone number"
            return (false, errorString)
        }
        guard let passwordText = password where passwordText.length > 5 else {
            errorString = "Please enter password minimum of six digits"
            return (false, errorString)
        }
       return (true, "Success on signup validation")
    }
    
    
    func validationForLogin(phoneNumber phoneNumber:String?, password:String?)  -> (isSuccess: Bool, error: String?)  {

        var errorString:String? = ""

        guard let phoneNumberText = phoneNumber where phoneNumberText.length > 0 else {
            errorString = "Please enter your phone number"
            return (false, errorString)
        }
        if phoneNumberText.length > 0 && phoneNumberText.length < 9 {
            errorString = "Please enter a valid phone number"
            return (false, errorString)
        }
        guard let passwordText = password where passwordText.length > 5 else {
            errorString = "Please enter password minimum of six digits"
            return (false, errorString)
        }

        return (true, "Success on Login validation")
    }
    
}