//
//  Profile.swift
//  Sportify
//
//  Created by Amit on 20/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import ObjectMapper

class Profile : Mappable {
    
    var data:ProfileData?
    var error:ResponseError?
    var token:String?
    var status:Bool?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        error <- map["error"]
        token <- map["token"]
        status <- map["status"]
    }
}


class ProfileData : Mappable {
    
    var userId:Int?
    var countryCode:String?
    var mobileNumber:String?
    var name:String?
    var email:String?
    var profileImage:String?
    var userStatus:String?
    var gender:String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        
        userId <- map["customerId"]
        countryCode <- map["customerCountryCode"]
        mobileNumber <- map["customerMobile"]
        name <- map["customerName"]
        email <- map["customerEmail"]
        profileImage <- map["customerProfileImage"]
        userStatus <- map["customerStatus"]
        gender <- map["customerGender"]
        
    }
}


