//
//  Error.swift
//  Sportify
//
//  Created by Amit on 16/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseError: Mappable {
  
    var code: Int?
    var message: String?

    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
    }
}


class SportifyError {
   
    var domain: String?
    var errorCode: Int?
    var erroMessage: String?

    init(domain: String, code:Int, error:String) {
        self.domain = domain
        self.errorCode = code
        self.erroMessage = error
    }
}