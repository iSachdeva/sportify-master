//
//  SignIn_ViewController.swift
//  Sportify
//
//  Created by Amit on 30/05/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SignIn_ViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, AccountEngine_Delegate{
    
    @IBOutlet weak var phoneNumberTextField:PaddingTextField!
    @IBOutlet weak var passwordTextField:PaddingTextField!
    
    var accountEngine: AccountEngine!
    var facebookInfo: NSMutableDictionary?
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookInfo = NSMutableDictionary()
        accountEngine  = AccountEngine(delegate_: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true;
    }
    
    //MARK:- IBAction Method
    @IBAction func facebookConnectClicked(sender:UIButton) {
        self.view.endEditing(true)
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //fbLoginManager.loginBehavior = FBSDKLoginBehavior.Browser
        
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self, handler: { (fbloginresult:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            
            if (error == nil){
                if(fbloginresult.isCancelled) {
                    //Show Cancel alert
                    return;
                } else /*if (fbloginresult.grantedPermissions.contains("email"))*/ {
                    self.returnUserData()
                    //fbLoginManager.logOut()
                }
            } else {
                FBSDKLoginManager().logOut()
            }
        })
    }
    
    
    @IBAction func signInClicked(sender:UIButton) {
        self.view.endEditing(true)
        
        let validationResult = accountEngine.validationForLogin(phoneNumber: phoneNumberTextField.text, password: passwordTextField.text)
        if validationResult.isSuccess {
            
            KVNProgress.show()
            accountEngine.login(phoneNumber: self.phoneNumberTextField.text!, password: self.passwordTextField.text!)
            
        } else {
            UIAlertController.present(title: "Error", message: validationResult.error, actionTitles: ["OK"]) { (action) -> () in
            }
        }
    }
    
    @IBAction func signUpClicked(sender:UIButton) {
        self.view.endEditing(true)
        
        var viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        viewControllers.removeAtIndex(viewControllers.count - 1)
        
        let signUpViewController = self.viewControllerWithStoryboardId("SignUpViewController_Id") as! SignUpWithMobile_ViewController
        signUpViewController.facebookInfo = self.facebookInfo

        viewControllers.append(signUpViewController)
        
        //  self.navigationController!.setViewControllers(viewControllers, animated: true);
        self.navigationController!.setViewControllers(viewControllers)
    }
    
    
    @IBAction func forgotPasswordClicked(sender:UIButton) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func backButtonClicked(sender:UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.pop()
        
        //   self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- UITextField Delegate Method
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 1 {
            passwordTextField.becomeFirstResponder()
            return false
        } else {
            textField.resignFirstResponder();
            return true
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1 && (range.location == 0 || range.location == 1 || range.location == 2 || range.location > 11 || string.length > 11  || string == ";" || string == "," || string == "+") {
            return false;
        } else {
            return true
        }
    }
    
    
    func returnUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil) {
            
            KVNProgress.show()
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil) {
                    print(result)
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.facebookInfo(result)
                        let facebookID = result.valueForKey("id") as! String
                        self.accountEngine.loginOrSignupWithFacebook(facebookID)
                    }
                    FBSDKLoginManager().logOut()
                } else {
                    //error occured while fb fetch data
                    KVNProgress.showErrorWithStatus(error.description)
                }
            })
        }
    }
    
    //MARK:- Account Engine Delegate methods
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:AccountEngineRequestType) {
        
        switch requestType {
        case .facebookLogin:
            
            KVNProgress.dismissWithCompletion({
                
                let facebookLogin = response as! FacebookLogin
                let isSignupActiveResponse = facebookLogin.isSignupActive
                if let isSignupActive = isSignupActiveResponse {
                    if(isSignupActive) {
                        self.moveToSignUpScreen()
                    } else {
                        UIAlertController.present(title: "", message: "Successfully login", actionTitles: ["OK"]) { (action) -> () in }
                    }
                }
            })
            
            break
            
        case .login:
            
            KVNProgress.dismissWithCompletion({
                UIAlertController.present(title: "", message: "Successfully Login", actionTitles: ["OK"]) { (action) -> () in }
            })
            break
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse:SportifyError, requestType:AccountEngineRequestType) {
        
        KVNProgress.dismissWithCompletion({
            UIAlertController.present(title: "Error", message: errorResponse.erroMessage, actionTitles: ["OK"]) { (action) -> () in }
        })
    }
    
    //MARK:- Helper methods
    func facebookInfo(result:AnyObject) {
        
        let facebookID = result.valueForKey("id") as! String
        let emailID = result.valueForKey("email") as! String
        let firstName = result.valueForKey("first_name") as! String
        let lastName = result.valueForKey("last_name") as! String
        let name = firstName + " " + lastName
        
        self.facebookInfo?.removeAllObjects()
        self.facebookInfo?.setValue(facebookID, forKey: "facebookId")
        self.facebookInfo?.setValue(name, forKey: "name")
        
        if !emailID.isEmpty{
            self.facebookInfo?.setValue(emailID, forKey: "emailId")
        }
    }
    
    func moveToSignUpScreen() {
        self.signUpClicked(UIButton())
    }
    
    //MARK:- Navigation Delegate Method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SignInToSignUpScreen_Id"{
            let signUpViewController = segue.destinationViewController as! SignUpWithMobile_ViewController
            signUpViewController.facebookInfo = self.facebookInfo
        } else {
            
        }
    }
    
    
}
