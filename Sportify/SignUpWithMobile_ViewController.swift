//
//  SignUpWithMobile_ViewController.swift
//  Sportify
//
//  Created by Amit on 01/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class SignUpWithMobile_ViewController: UIViewController, UITextFieldDelegate, AccountEngine_Delegate {
    
    @IBOutlet var nameTextField: PaddingTextField!
    @IBOutlet var phoneNumberTextField: PaddingTextField!
    @IBOutlet var passwordTextField: PaddingTextField!
    var accountEngine: AccountEngine!
    
    var facebookInfo: NSDictionary?
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setFacebbokName()
        accountEngine  = AccountEngine(delegate_: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true;
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setFacebbokName() {
        
        if let facebookInfo = self.facebookInfo {
            if let name = facebookInfo.valueForKey("name") as? String {
                self.nameTextField.text = name
            }
        }
    }
    
    //MARK:- UITextField Delegate Method
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 1 {
            phoneNumberTextField.becomeFirstResponder()
            return false
        } else if(textField.tag == 2) {
            passwordTextField.becomeFirstResponder()
            return false
        } else if(textField.tag == 3) {
            textField.resignFirstResponder();
            return true
        } else {
            textField.resignFirstResponder();
            return true
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 2 && (range.location == 0 || range.location == 1 || range.location == 2 || range.location > 11 || string.length > 11 || string == ";" || string == "," || string == "+") {
            return false;
        } else {
            return true
        }
    }
    
    @IBAction func submitClicked(sender:UIButton) {
       
        self.view.endEditing(true)
        
        let validationResult = accountEngine.validationForSignup(name: nameTextField.text, phoneNumber: phoneNumberTextField.text, password: passwordTextField.text)
        if validationResult.isSuccess {
            
            var facebookId:String?
            var emailID:String?
            
            if let theFacebookInfo = self.facebookInfo {
                facebookId = theFacebookInfo.objectForKey("facebookId") as? String
                emailID = theFacebookInfo.objectForKey("emailId") as? String
            }
            
            KVNProgress.show()
            accountEngine.signup(facebookId, name: self.nameTextField.text!, phoneNumber: self.phoneNumberTextField.text!, password: self.passwordTextField.text!, email: emailID)
            
        } else {
            UIAlertController.present(title: "Error", message: validationResult.error, actionTitles: ["OK"]) { (action) -> () in
            }
        }
        //self.performSegueWithIdentifier("SignUpToOTPScreen_Id", sender: nil)
    }
    
    //MARK:- IBAction Method
    @IBAction func signInClicked(sender:UIButton) {
        
        self.resignFirstResponder()
        
        var viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        viewControllers.removeAtIndex(viewControllers.count - 1)
        
        let signInViewController = self.viewControllerWithStoryboardId("SignInViewController_Id") as! SignIn_ViewController
        
        viewControllers.append(signInViewController)
        
        self.navigationController!.setViewControllers(viewControllers)
    }
    
    
    //MARK:- Navigation Delegate Method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SignUpToOTPScreen_Id" {
            let otpViewController = segue.destinationViewController as! OTP_ViewController
            otpViewController.signupData = sender as? SignupData
      
        } else {
            
        }
    }
    
    //MARK:- Helper Methods
    
    
    
    //MARK:- Account Engine Delegate methods
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:AccountEngineRequestType) {
        
        KVNProgress.dismiss()

        switch requestType {
        case .signUp:
            
            let signupResponse = response as! Signup
            self.performSegueWithIdentifier("SignUpToOTPScreen_Id", sender: signupResponse.data)
            
            break
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse:SportifyError, requestType:AccountEngineRequestType) {
        
        switch requestType {
        case .signUp:
            KVNProgress.dismissWithCompletion({
                UIAlertController.present(title: "Error", message: errorResponse.erroMessage, actionTitles: ["OK"]) { (action) -> () in }
            })
            break
            
        default:
            break
        }
    }
}




