//
//  Landing_ViewController.swift
//  Sportify
//
//  Created by Amit on 27/05/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class Landing_ViewController: UIViewController,AccountEngine_Delegate {
    
    var accountEngine: AccountEngine!
    var facebookInfo: NSMutableDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookInfo = NSMutableDictionary()
        accountEngine  = AccountEngine(delegate_: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true;
    }
    
    @IBAction func signUpWithMobileClicked(sender:UIButton) {
        
        let signUpViewController = self.viewControllerWithStoryboardId("SignUpViewController_Id") as! SignUpWithMobile_ViewController
        self.navigationController?.push(viewController: signUpViewController)
        
       // self.performSegueWithIdentifier("LandingToSignUpWithMobileScreen_Id", sender: nil)
    }
    
    @IBAction func facebookConnectClicked(sender:UIButton) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //fbLoginManager.loginBehavior = FBSDKLoginBehavior.Browser
        
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self, handler: { (fbloginresult:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            
            if (error == nil){
                if(fbloginresult.isCancelled) {
                    //Show Cancel alert
                    return;
                } else /*if (fbloginresult.grantedPermissions.contains("email"))*/ {
                    self.returnUserData()
                    //fbLoginManager.logOut()
                }
            } else {
                FBSDKLoginManager().logOut()
            }
        })
    }
    
    func returnUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil) {
            
            KVNProgress.show()
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil) {
                    print(result)
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.facebookInfo(result)
                    let facebookID = result.valueForKey("id") as! String
                    self.accountEngine.loginOrSignupWithFacebook(facebookID)
                    }
                    FBSDKLoginManager().logOut()
                } else {
                    //error occured while fb fetch data
                    KVNProgress.showErrorWithStatus(error.description)
                }
            })
        }
    }
    
    @IBAction func signInClicked(sender:UIButton) {
        
        let signInViewController = self.viewControllerWithStoryboardId("SignInViewController_Id") as! SignIn_ViewController
        self.navigationController?.push(viewController: signInViewController)
        //self.performSegueWithIdentifier("LandingToSignInScreen_Id", sender: nil)
    }
    

    //MARK:- Account Engine Delegate methods
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:AccountEngineRequestType) {
    
        switch requestType {
        case .facebookLogin:
            
            KVNProgress.dismissWithCompletion({
                let facebookLogin = response as! FacebookLogin
                let isSignupActiveResponse = facebookLogin.isSignupActive
                if let isSignupActive = isSignupActiveResponse {
                    if(isSignupActive) {
                        self.moveToSignUpScreen()
                    } else {
                        UIAlertController.present(title: "", message: "Successfully login", actionTitles: ["OK"]) { (action) -> () in }
                    }
                }
            })
            break
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse:SportifyError, requestType:AccountEngineRequestType) {
        
        switch requestType {
        case .facebookLogin:
            KVNProgress.dismissWithCompletion({
                UIAlertController.present(title: "Error", message: errorResponse.erroMessage, actionTitles: ["OK"]) { (action) -> () in }
            })
            break
            
        default:
            break
        }
    }
     
    //MARK:- Helper methods
    func facebookInfo(result:AnyObject) {
        
        let facebookID = result.valueForKey("id") as! String
        let emailID = result.valueForKey("email") as! String
        let firstName = result.valueForKey("first_name") as! String
        let lastName = result.valueForKey("last_name") as! String
        let name = firstName + " " + lastName
        
        self.facebookInfo?.removeAllObjects()
        self.facebookInfo?.setValue(facebookID, forKey: "facebookId")
        self.facebookInfo?.setValue(name, forKey: "name")
      
        if !emailID.isEmpty{
            self.facebookInfo?.setValue(emailID, forKey: "emailId")
        }
    }
    
    func moveToSignUpScreen() {
        self.performSegueWithIdentifier("LandingToSignUpWithMobileScreen_Id", sender: nil)
    }
    
    //MARK:- Navigation Delegate Method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "LandingToSignInScreen_Id" {
            
        } else if segue.identifier == "LandingToSignUpWithMobileScreen_Id"{
            let signUpViewController = segue.destinationViewController as! SignUpWithMobile_ViewController
            signUpViewController.facebookInfo = self.facebookInfo
        } else {
            
        }
    }
    
    
}
