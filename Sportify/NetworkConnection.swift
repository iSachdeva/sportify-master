//
//  NetworkConnection.swift
//  Sportify
//
//  Created by Amit on 13/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper

enum NetworkRequestType {
    case  loginOrSignUpWithFacebook
    case  signUpWithMobileNumber
    case  otpVerification
    case  loginWithMobileNumber
}

protocol NetworkConnection_Delegate {
    
    func networkConnectionDidSuccessWithResponse(response:AnyObject?,requestType:NetworkRequestType)
    func networkConnectionDidFailedWithErrorResponse(response:SportifyError?,requestType:NetworkRequestType)
}

class NetworkConnection: NSObject {
    
    var delegate:NetworkConnection_Delegate?
    
    init(delegate_:NetworkConnection_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    
    /* func requestTologinOrSignupByFacebookID334(postParameters:NSDictionary)  {
     
     let url: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.facebookLoginOrSignup
     
     Alamofire.request(.POST, url, parameters:postParameters as? [String : AnyObject] , encoding:.JSON).responseJSON
     { response in switch response.result {
     
     case .Success(let JSON):
     
     if let lDelegate:NetworkConnection_Delegate = self.delegate {
     print(JSON)
     lDelegate.networkConnectionDidSuccessWithResponse(JSON, requestType: .loginOrSignUpWithFacebook)
     }
     case .Failure(let error):
     if let lDelegate:NetworkConnection_Delegate = self.delegate {
     print(error)
     lDelegate.networkConnectionDidFailedWithErrorResponse(error, requestType: .loginOrSignUpWithFacebook)
     }
     }
     }
     }*/
    
    //MARK:- Login by Facebook
    func requestTologinOrSignupByFacebookID(postParameters:NSDictionary)  {
        
        let URL: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.facebookLoginOrSignup
        
        Alamofire.request(.POST, URL, parameters:postParameters as? [String : AnyObject], encoding:.JSON).responseObject { (response: Response<FacebookLogin, NSError>) in
            
            switch response.result {
            case .Success:
                let facebookLoginResponse = response.result.value
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    lDelegate.networkConnectionDidSuccessWithResponse(facebookLoginResponse, requestType: .loginOrSignUpWithFacebook)
                }
                break
                
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                     let responseError = self.createError(forDomain: Constants.DomainName.signup, responseError: error)
                    lDelegate.networkConnectionDidFailedWithErrorResponse(responseError, requestType: .loginOrSignUpWithFacebook)
                }
                break;
                
            }
        }
    }
    
    //MARK:- Signup with mobile number
    func requestToSignupWithMobileNumber(postParameters:NSDictionary)  {
        
        let URL: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.signup
        
        Alamofire.request(.POST, URL, parameters:postParameters as? [String : AnyObject], encoding:.JSON).responseObject { (response: Response<Signup, NSError>) in
            
            switch response.result {
            
            case .Success:
                
                if let signupResponse = response.result.value {
                    
                    if signupResponse.status == true {
                        self.delegate!.networkConnectionDidSuccessWithResponse(signupResponse, requestType: .signUpWithMobileNumber)
                    } else {
                        let error = self.createError(forDomain: Constants.DomainName.signup, responseError: signupResponse.error)
                        self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .signUpWithMobileNumber)
                    }
                } else {
                    let error = self.nilValueErrorResponse(forDomain: Constants.DomainName.signup)
                    self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .signUpWithMobileNumber)
                }
                break
                
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    let responseError = self.createError(forDomain: Constants.DomainName.signup, responseError: error)
                    lDelegate.networkConnectionDidFailedWithErrorResponse(responseError, requestType: .signUpWithMobileNumber)
                }
                break;
            }
        }
    }
    
    //MARK:- Verify OTP
    func requestToVerifyOTP(postParameters:NSDictionary)  {
        
        let URL: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.otpVerification
        
        Alamofire.request(.POST, URL, parameters:postParameters as? [String : AnyObject], encoding:.JSON).responseObject { (response: Response<Profile, NSError>) in
            
            switch response.result {
                
            case .Success:
                if let otpVerificationResponse = response.result.value {
            
                    if otpVerificationResponse.status == true {
                        self.delegate!.networkConnectionDidSuccessWithResponse(otpVerificationResponse, requestType: .otpVerification)
                    } else {
                        let error = self.createError(forDomain: Constants.DomainName.otpVerification, responseError: otpVerificationResponse.error)
                        self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .otpVerification)
                    }
                } else {
                    let error = self.nilValueErrorResponse(forDomain: Constants.DomainName.otpVerification)
                    self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .otpVerification)
                }
                break
                
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    let responseError = self.createError(forDomain: Constants.DomainName.otpVerification, responseError: error)
                    lDelegate.networkConnectionDidFailedWithErrorResponse(responseError, requestType: .otpVerification)
                }
                break;
            }
        }
    }
    
    
    //MARK:- Login
    func requestToLogin(postParameters:NSDictionary)  {
        
        let URL: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.login
        
        Alamofire.request(.POST, URL, parameters:postParameters as? [String : AnyObject], encoding:.JSON).responseObject { (response: Response<Profile, NSError>) in
            
            switch response.result {
                
            case .Success:
                if let loginResponse = response.result.value {
                    
                    if loginResponse.status == true {
                        self.delegate!.networkConnectionDidSuccessWithResponse(loginResponse, requestType: .loginWithMobileNumber)
                    } else {
                        let error = self.createError(forDomain: Constants.DomainName.login, responseError: loginResponse.error)
                        self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .loginWithMobileNumber)
                    }
                } else {
                    let error = self.nilValueErrorResponse(forDomain: Constants.DomainName.login)
                    self.delegate!.networkConnectionDidFailedWithErrorResponse(error, requestType: .loginWithMobileNumber)
                }
                break
                
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    let responseError = self.createError(forDomain: Constants.DomainName.login, responseError: error)
                    lDelegate.networkConnectionDidFailedWithErrorResponse(responseError, requestType: .loginWithMobileNumber)
                }
                break;
            }
        }
    }

    
    
    //MARK:- Helper Methods
    func nilValueErrorResponse(forDomain domain:String) -> SportifyError {
        
        let code = 801;
        let errorMessage = Constants.Message.somethingWrongError
        
        let error = SportifyError(domain:domain, code: code, error: errorMessage)
        return error
    }
    
    func createError(forDomain domain:String, responseError:AnyObject?) -> SportifyError {
        
        var code = 0;
        var errorMessage = ""
        
        if let error = responseError {
            
            if error is ResponseError {
                
                if let erroResponse = error as? ResponseError {
                    
                    if let theCode = erroResponse.code {
                        code = theCode
                    }
                    
                    if let theMessage = erroResponse.message {
                        errorMessage = theMessage
                    }
                } else {
                    code = 801
                    errorMessage = Constants.Message.somethingWrongError
                }
                
            } else if error is NSError {
                
                if let theError = error as? NSError {
                    code = theError.code
                    errorMessage = theError.localizedDescription
                } else {
                    code = 801
                    errorMessage = Constants.Message.somethingWrongError
                }
                
            } else {
                code = 801
                errorMessage = Constants.Message.somethingWrongError
            }
        } else {
            code = 801
            errorMessage = Constants.Message.somethingWrongError
        }
        
        let error = SportifyError(domain:domain, code: code, error: errorMessage)
        return error
    }
    
    //MARK:- ETC Methods
 /*   func requestToVerifyOTP(postParameters:NSDictionary)  {
     
     let URL: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.otpVerification
     
     Alamofire.request(.POST, URL, parameters:postParameters as? [String : AnyObject] , encoding:.JSON).responseJSON { response in
     
     switch response.result {
     
     case .Success(let JSON):
     print(JSON)
     case .Failure(let error):
     print(error)
     }
     }
     }
    */
    
}