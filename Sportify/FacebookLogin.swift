//
//  FacebookLogin.swift
//  Sportify
//
//  Created by Amit on 13/06/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import ObjectMapper

class FacebookLogin: Mappable {
    var data: ProfileData?
    var message:String?
    var error:ResponseError?
    var isLoginActive:Bool?
    var isSignupActive:Bool?
    var status:Bool?
    
    required init?(_ map: Map){
      
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        message <- map["message"]
        error <- map["error"]
        isLoginActive <- map["isLoginActive"]
        isSignupActive <- map["isSignupActive"]
        status <- map["status"]
    }
}

